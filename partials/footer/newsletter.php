<div class="newsletter">
	
	<h4>We’re dreaming up new ideas every day, sign up for our newsletter for updates</h4>

	<div class="cta">
		<a href="#" class="btn" id="subscribe">Stay Connected</a>
	</div>

</div>